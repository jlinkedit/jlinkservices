package eu.jlinkeditrdfconverter.owl;

import java.io.Serializable;


public enum Countries implements Serializable{

	Austria("Austria"), Belgium("Belgium"), Bulgaria("Bulgaria"), Croatia(
			"Croatia"), Cyprus("Cyprus"), Czech_Republic("Czech Republic"), Denmark(
			"Denmark"), Estonia("Estonia"), Finland("Finland"), France(
			"France"), Germany("Germany"), United_Kingdom("United Kingdom"), Greece(
			"Greece"), Hungary("Hungary"), Iceland("Iceland"), Ireland(
			"Ireland"), Italy("Italy"), Kosovo("Kosovo"), Latvia("Latvia"), Lithuania(
			"Lithuania"), Luxembourg("Luxembourg"), Macedonia(
			"FYR of Macedonia"), Malta("Malta"), Netherlands("Netherlands"), Norway(
			"Norway"), Poland("Poland"), Portugal("Portugal"), Romania(
			"Romania"), Serbia("Serbia"), Slovakia("Slovakia"), Slovenia(
			"Slovenia"), Spain("Spain"), Sweden("Sweden"), Switzerland(
			"Switzerland"), Ukraine("Ukraine");

	private String name;

	Countries(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}

	public static String getEnumNameForValue(String name) {
		Countries[] values = Countries.values();
		String enumValue = null;
		for (Countries eachValue : values) {
			enumValue = eachValue.toString();
			if (name.equals(eachValue)) {
				return eachValue.name();
			}
		}
		return enumValue;

	}

}

