package eu.jlinkeditrdfconverter.owl.builder;

import eu.jlinkeditrdfconverter.owl.Countries;
import eu.jlinkeditrdfconverter.owl.Europe;

public class RDFBuilder {
	
	
	public String EuropeRDFBuilder(){
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n"+
		"<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" \n"+
		"xmlns:dc=\"http://purl.org/dc/elements/1.1/\" > \n");
		try{
			Europe europe = new Europe();
			sb.append("<rdf:Description rdf:about=\""+BuilderUtil.getAbout(europe)+"\"> \n");
			
			for(Countries countryName:Countries.values()){
				sb.append("<dc:hasCountry>"+countryName+"</dc:hasCountry> \n ");
			}
			
			sb.append("</rdf:Description> \n ");
			
		}finally{
			sb.append("</rdf:RDF> \n");
		}
		return sb.toString();
	}
	
	
	
	//TODO create a test case with RDF below.
//	public static void main(String[] args) {
//		System.out.println(new RDFBuilder().EuropeRDFBuilder());
//	}
//	
//	
//	<?xml version="1.0" encoding="UTF-8"?> 
//	<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
//	xmlns:dc="http://purl.org/dc/elements/1.1/" > 
//	<rdf:Description rdf:about="http://aims.fao.org/aos/geopolitical.owl#Europe"> 
//	<dc:name>Austria</dc:name> 
//	 <dc:name>Belgium</dc:name> 
//	 <dc:name>Bulgaria</dc:name> 
//	 <dc:name>Croatia</dc:name> 
//	 <dc:name>Cyprus</dc:name> 
//	 <dc:name>Czech Republic</dc:name> 
//	 <dc:name>Denmark</dc:name> 
//	 <dc:name>Estonia</dc:name> 
//	 <dc:name>Finland</dc:name> 
//	 <dc:name>France</dc:name> 
//	 <dc:name>Germany</dc:name> 
//	 <dc:name>United Kingdom</dc:name> 
//	 <dc:name>Greece</dc:name> 
//	 <dc:name>Hungary</dc:name> 
//	 <dc:name>Iceland</dc:name> 
//	 <dc:name>Ireland</dc:name> 
//	 <dc:name>Italy</dc:name> 
//	 <dc:name>Kosovo</dc:name> 
//	 <dc:name>Latvia</dc:name> 
//	 <dc:name>Lithuania</dc:name> 
//	 <dc:name>Luxembourg</dc:name> 
//	 <dc:name>FYR of Macedonia</dc:name> 
//	 <dc:name>Malta</dc:name> 
//	 <dc:name>Netherlands</dc:name> 
//	 <dc:name>Norway</dc:name> 
//	 <dc:name>Poland</dc:name> 
//	 <dc:name>Portugal</dc:name> 
//	 <dc:name>Romania</dc:name> 
//	 <dc:name>Serbia</dc:name> 
//	 <dc:name>Slovakia</dc:name> 
//	 <dc:name>Slovenia</dc:name> 
//	 <dc:name>Spain</dc:name> 
//	 <dc:name>Sweden</dc:name> 
//	 <dc:name>Switzerland</dc:name> 
//	 <dc:name>Ukraine</dc:name> 
//	 </rdf:Description> 
//	 </rdf:RDF> 


}
