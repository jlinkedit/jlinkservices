package eu.jlinkeditrdfconverter.owl.builder;

import java.lang.annotation.Annotation;

import eu.jlinkeditrdfconverter.annotation.Subject;


public class BuilderUtil {

	public static String getAbout(Object obj){
		
		for(Annotation annotation : obj.getClass().getDeclaredAnnotations()){
			if(annotation instanceof eu.jlinkeditrdfconverter.annotation.Subject){
				eu.jlinkeditrdfconverter.annotation.Subject subject =  (Subject) annotation;
				return subject.about();
			}
		}
		
		return null;
	}
	
	
}
