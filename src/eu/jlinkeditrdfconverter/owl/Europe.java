package eu.jlinkeditrdfconverter.owl;

import eu.jlinkeditrdfconverter.annotation.Predicate;
import eu.jlinkeditrdfconverter.annotation.Subject;



@Subject(about="http://aims.fao.org/aos/geopolitical.owl#Europe")
public class Europe {
	
	@Predicate(value="this")
	public Countries country;

	public Countries getCountry() {
		return country;
	}

	public void setCountry(Countries country) {
		this.country = country;
	}
	
	

}
