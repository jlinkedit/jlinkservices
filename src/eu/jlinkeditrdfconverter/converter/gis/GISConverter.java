package eu.jlinkeditrdfconverter.converter.gis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.DriverManager;

public class GISConverter {
	
//	public static void main(String[] args) { 
//
//		  java.sql.Connection conn; 
//
//		  try { 
//		    /* 
//		    * Load the JDBC driver and establish a connection. 
//		    */
//		    Class.forName("org.postgresql.Driver"); 
//		    String url = "jdbc:postgresql://localhost:5432/postgres"; 
//		    conn = DriverManager.getConnection(url, "postgres", "root"); 
//		    /* 
//		    * Add the geometry types to the connection. Note that you 
//		    * must cast the connection to the pgsql-specific connection 
//		    * implementation before calling the addDataType() method. 
//		    */
//		    ((org.postgresql.Connection)conn).addDataType("geometry","org.postgis.PGgeometry")
//		;
//		    ((org.postgresql.Connection)conn).addDataType("box3d","org.postgis.PGbox3d");
//		    /* 
//		    * Create a statement and execute a select query. 
//		    */ 
//		    Statement s = conn.createStatement(); 
//		    ResultSet r = s.executeQuery("select AsText(geom) as geom,id from geomtable"); 
//		    while( r.next() ) { 
//		      /* 
//		      * Retrieve the geometry as an object then cast it to the geometry type. 
//		      * Print things out. 
//		      */ 
//		      PGgeometry geom = (PGgeometry)r.getObject(1); 
//		      int id = r.getInt(2); 
//		      System.out.println("Row " + id + ":");
//		      System.out.println(geom.toString()); 
//		    } 
//		    s.close(); 
//		    conn.close(); 
//		  } 
//		catch( Exception e ) { 
//		  e.printStackTrace(); 
//		  } 
//		} 

    public static void main(String args[]) 
    { 
        try 
        { 
        	String path = "C:/Development/geographical_distribution/shinyWebApp/Census2011_NUTS3_generalised20m/Census2011_NUTS3_generalised20m.shp";
            Process p=Runtime.getRuntime().exec("cmd");
            p=Runtime.getRuntime().exec("C:/Users/solver/workspace/linkedit/JLinkeditRDFConverter/WebContent/WEB-INF/uploadGis.bat "+path);
//            p=Runtime.getRuntime().exec("psql -h sds2.itc.virginia.edu -U postgres -f bellezza.sql -d thdl_places_production");
//            p=Runtime.getRuntime().exec("psql -h sds2.itc.virginia.edu -U postgres -d thdl_places_production ");
//            p=Runtime.getRuntime().exec("-c "+ "\"GRANT ALL ON bellezza TO rubyuser\"");
            
            p.waitFor(); 
            BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream())
            ); 
            String line; 
            while((line = reader.readLine()) != null) 
            { 
                System.out.println(line);
            } 

        }
        catch(IOException e1) {} 
        catch(InterruptedException e2) {} 

        System.out.println("Done"); 
    } 
}
