package eu.jlinkeditrdfconverter.entity;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.http.converter.json.GsonBuilderUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.jvisualrdf.graph.json.in.GraphJsonIn;
import eu.jvisualrdf.graph.json.out.GraphJsonOut;

@Document
public class UserSessionEntity implements Serializable{
	private static final long serialVersionUID = 5055715636296431638L;

	private ObjectId _id;

	private String token;
	
	private String graphAsJsonStr;
	
	
	public ObjectId get_id() {
		return _id;
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getGraphAsJsonStr() {
		return graphAsJsonStr;
	}

	public void setGraphAsJsonStr(String graphAsJsonStr) throws ParseException {
		if(graphAsJsonStr==null || "".equals(graphAsJsonStr)){
			return;
		}
		
		JSONParser parser = new JSONParser();
		
		JSONObject jsonObj = (JSONObject) parser.parse(graphAsJsonStr);
		Gson gson = new GsonBuilder().create();
		GraphJsonIn graphin = null;
		GraphJsonOut graphout = null;

		JSONArray nodesArr = (JSONArray) parser.parse(jsonObj.get("nodes").toString());
		JSONObject json = (JSONObject) parser.parse(nodesArr.get(0).toString());
		//System.out.println(json.containsKey("id"));
		
		if(json.containsKey("x")|| json.containsKey("y") || json.containsKey("px") || json.containsKey("py")){
			graphin = gson.fromJson(graphAsJsonStr, GraphJsonIn.class);			
			graphout = new GraphJsonOut(graphin.getNodes(),graphin.getLinks());	
			this.graphAsJsonStr = gson.toJson(graphout);
			return;
		}
		//System.out.println(graphAsJsonStr);
		graphout = gson.fromJson(graphAsJsonStr, GraphJsonOut.class);		
		this.graphAsJsonStr = gson.toJson(graphout);
	}

	
	
	
}
