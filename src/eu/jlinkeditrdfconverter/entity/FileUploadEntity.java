package eu.jlinkeditrdfconverter.entity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
@XmlRootElement
public class FileUploadEntity implements Serializable {
	private static final long serialVersionUID = 567403388897540093L;

	private ObjectId _id;
	
	private String token;

	private List<String> entries;
	
	private String description;
	
	private String detail;
	
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public ObjectId get_id() {
		return _id;
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public List<String> getEntries() {
		return entries;
	}

	public void setEntries(List<String> entries) {
		this.entries = entries;
	}
	
	public void loadFrom(InputStream inputStream) throws IOException{
		List<String> entries = new LinkedList<String>();
		
		BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
		String line = r.readLine();
		while (line != null){
			String[] cells  = line.split(";");
			if (cells.length > 0 && cells[0].trim().length() > 0)
				entries.add(cells[0].trim());
			line = r.readLine();
		}
		r.close();
		
		setEntries(entries);
	}

	@Override
	public String toString() {
		return "FileUploadEntity [_id=" + _id + ", token=" + token + ", entries=" + entries + ", description=" + description + ", detail=" + detail + "]";
	}
	
	

}
