package eu.jlinkeditrdfconverter.entity;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

public class NodeEntity  implements Serializable{
	private static final long serialVersionUID = 4682610087540708510L;
	
	@Expose
	private String name;
	@Expose
	private String uri;
	@Expose
	private String type;
	@Expose
	private Boolean reflexive;
	@Expose
	private Integer id;
	@Expose
	private Integer x;
	@Expose
	private Integer y;


	public NodeEntity(){}
	
	public NodeEntity(String name, String uri, String type, Boolean reflexive, Integer id, Integer x, Integer y) {
		super();
		this.name = name;
		this.uri = uri;
		this.type = type;
		this.reflexive = reflexive;
		this.id = id;
		this.x = x;
		this.y = y;
	}

	public NodeEntity(eu.jvisualrdf.graph.json.in.Node nodein){
		  this.name = nodein.getName();
		  this.uri = nodein.getUri();
		  this.type = nodein.getType();
		  this.reflexive = nodein.getReflexive();
		  this.id = nodein.getId();
	}
	
	/**
	 * 
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return The uri
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * 
	 * @param uri
	 *            The uri
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * 
	 * @return The type
	 */
	public String getType() {
		return type;
	}

	/**
	 * 
	 * @param type
	 *            The type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 
	 * @return The reflexive
	 */
	public Boolean getReflexive() {
		return reflexive;
	}

	/**
	 * 
	 * @param reflexive
	 *            The reflexive
	 */
	public void setReflexive(Boolean reflexive) {
		this.reflexive = reflexive;
	}

	/**
	 * 
	 * @return The id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return "NodeEntity [name=" + name + ", uri=" + uri + ", type=" + type + ", reflexive=" + reflexive + ", id=" + id + ", x=" + x + ", y=" + y + "]";
	}



	
}
