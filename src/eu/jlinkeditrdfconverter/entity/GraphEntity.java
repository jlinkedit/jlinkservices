package eu.jlinkeditrdfconverter.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.google.gson.annotations.Expose;

@Document
@XmlRootElement
public class GraphEntity implements Serializable{
	private static final long serialVersionUID = 4682610087540708510L;
	
	private List<NodeEntity> nodes = new ArrayList<NodeEntity>();
	private List<LinkEntity> links = new ArrayList<LinkEntity>();
	
	private ObjectId _id;
	private String token;
	private String name;
	private String detail;
	private Boolean draft;

	public List<NodeEntity> getNodes() {
		return nodes;
	}

	public void setNodes(List<NodeEntity> nodes) {
		this.nodes = nodes;
	}

	public List<LinkEntity> getLinks() {
		return links;
	}

	public void setLinks(List<LinkEntity> links) {
		this.links = links;
	}

	public ObjectId get_id() {
		return _id;
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Boolean getDraft() {
		return draft;
	}

	public void setDraft(Boolean draft) {
		this.draft = draft;
	}

	@Override
	public String toString() {
		return "GraphEntity [nodes=" + nodes + ", links=" + links + ", _id=" + _id + ", token=" + token + ", name=" + name + ", detail=" + detail + ", draft="
				+ draft + "]";
	}
	
	

	
}
