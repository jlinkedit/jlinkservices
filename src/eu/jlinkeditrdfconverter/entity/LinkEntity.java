package eu.jlinkeditrdfconverter.entity;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

public class LinkEntity  implements Serializable{
	private static final long serialVersionUID = 4682610087540708510L;

	@Expose
	private Integer source;
	@Expose
	private Integer target;
	@Expose
	private String name;
	@Expose
	private Boolean right;
	@Expose
	private Boolean felt;
	@Expose
	private Integer value;

	@SuppressWarnings("unused")
	public LinkEntity(){}
	
	
	
	public LinkEntity(Integer source, Integer target, String name, Boolean right, Boolean felt, Integer value) {
		super();
		this.source = source;
		this.target = target;
		this.name = name;
		this.right = right;
		this.felt = felt;
		this.value = value;
	}



	public LinkEntity(eu.jvisualrdf.graph.json.in.Link linkin){
		this.source = linkin.getSource().getId();
		this.target = linkin.getTarget().getId();
		this.name = linkin.getName();
		this.right = linkin.getRight();
		this.felt = linkin.getFelt();
		this.value = linkin.getValue();
	}


	public Integer getSource() {
		return source;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	public Integer getTarget() {
		return target;
	}

	public void setTarget(Integer target) {
		this.target = target;
	}

	/**
	 * 
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return The right
	 */
	public Boolean getRight() {
		return right;
	}

	/**
	 * 
	 * @param right
	 *            The right
	 */
	public void setRight(Boolean right) {
		this.right = right;
	}

	/**
	 * 
	 * @return The felt
	 */
	public Boolean getFelt() {
		return felt;
	}

	/**
	 * 
	 * @param felt
	 *            The felt
	 */
	public void setFelt(Boolean felt) {
		this.felt = felt;
	}

	/**
	 * 
	 * @return The value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * 
	 * @param value
	 *            The value
	 */
	public void setValue(Integer value) {
		this.value = value;
	}



	@Override
	public String toString() {
		return "LinkEntity [source=" + source + ", target=" + target + ", name=" + name + ", right=" + right + ", felt=" + felt + ", value=" + value + "]";
	}
	
	

}
