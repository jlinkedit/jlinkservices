package eu.jlinkeditrdfconverter.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.jvisualrdf.graph.json.in.GraphJsonIn;
import eu.jvisualrdf.graph.json.out.GraphJsonOut;

public class JLinkeditRDFConverterServiceUtil {
	
	public static GraphJsonIn convertGraphJsonIn(String graphJsonInStr){
		Gson gson = new GsonBuilder().create();
		GraphJsonIn graphJsonIn = gson.fromJson(graphJsonInStr, GraphJsonIn.class);
		return graphJsonIn;
	}

	public static GraphJsonOut parseGraphJsonInToOut(GraphJsonIn graphJsonIn) {
		GraphJsonOut graphJsonOut = new GraphJsonOut(graphJsonIn.getNodes(),graphJsonIn.getLinks());
		return graphJsonOut;
	}
	
	public static void main(String[] args) {
		String resultJson = "{\"nodes\": [{\"name\": \"g: visualRDF\", "+
		"\"uri\": \"http: \\\\graves.cl\\/visualRDF\", "+
		"\"type\": \"uri\", "+
		"\"reflexive\": true, "+
		"\"id\": 0 "+
    "}, "+
    "{ "+
        "\"name\": \"http: \\\\alvaro.graves.cl\", "+
        "\"uri\": \"http: \\\\alvaro.graves.cl\", "+
        "\"type\": \"uri\", "+
        "\"reflexive\": false, "+
		"\"id\": 1 "+
    "}, "+
    "{ "+
        "\"name\": \"http: \\\\github.com\\/alangrafu\\/visualRDF\", "+
        "\"uri\": \"http: \\\\github.com\\/alangrafu\\/visualRDF\", "+
        "\"type\": \"uri\", "+
        "\"reflexive\": false, "+
		"\"id\": 2 "+
	"}, "+
	"{ "+
        "\"name\": \"g:visualRDF\\/css\\/bootstrap-responsive.min.css\", "+
        "\"uri\": \"http: \\\\graves.cl\\/visualRDF\\/css\\/bootstrap-responsive.min.css\", "+
        "\"type\": \"uri\", "+
        "\"reflexive\": false, "+
		"\"id\": 3 "+
	"}, "+
	"{ "+
        "\"name\": \"g:visualRDF\\/css\\/bootstrap.min.css\", "+
        "\"uri\": \"http:\\\\graves.cl\\/visualRDF\\/css\\/bootstrap.min.css\", "+
        "\"type\": \"uri\", "+
        "\"reflexive\": false, "+
		"\"id\": 4 "+
		"}, "+
		"{ "+
            "\"name\": \"jbjares\", "+
            "\"uri\": \"http:\\\\jbjares\", "+
            "\"type\": \"uri\", "+
            "\"reflexive\": true, "+
			"\"id\": 5 "+
        "} "+
"], "+
"\"links\": [ "+
"{ "+
        "\"source\": 0, "+
        "\"target\": 1, "+
        "\"name\": \"dc: creator\", "+
        "\"right\": true, "+
        "\"felt\": false, "+
        "\"value\": 10 "+
	"}, "+
	"{ "+
        "\"source\": 0, "+
        "\"target\": 2, "+
        "\"name\": \"dc:source\", "+
        "\"right\": true, "+
        "\"felt\": false, "+
        "\"value\": 10 "+
	"}, "+
	"{ "+
        "\"source\": 0, "+
        "\"target\": 3, "+
        "\"name\": \"xhtml:stylesheet\", "+
        "\"right\": true, "+
        "\"felt\": false, "+
        "\"value\": 10 "+
	"}, "+
	"{ "+
        "\"source\": 0, "+
        "\"target\": 4, "+
        "\"name\": \"xhtml:stylesheet\", "+
        "\"right\": true, "+
        "\"felt\": false, "+
        "\"value\": 10 "+
    "} "+
"] "+
"} ";

//		Gson gson = new GsonBuilder().create();
//		GraphJsonIn graphIn = null;
//		GraphJsonOut graphOut = null;
//		graphIn = gson.fromJson(graphAsJson, GraphJsonIn.class);
//		graphOut = new GraphJsonOut(graphIn.getNodes(),graphIn.getLinks());
		
		String graphJsonInStr = "{\"nodes\":[ "
				+ "{\"name\":\"Europe\", "
				+ "\"uri\":\"http://aims.fao.org/aos/geopolitical.owl#Europe\", "
				+ "\"type\":\"Resource\", "
				+ "\"reflexive\":true, "
				+ "\"id\":0 "
			+ "}, "
			+ "{ "
				+ "\"name\":\"Ireland\", "
				+ "\"uri\":\"\", "
				+ "\"type\":\"value\", "
				+ "\"reflexive\":false, "
				+ "\"id\":1 "
			+ "}, "
			+ "{ "
				+ "\"name\":\"United_Kingdom\", "
				+ "\"uri\":\"\", "
				+ "\"type\":\"value\","
				+ "\"reflexive\":false,"
				+ "\"id\":2"
				+ "}"
				+ "],"
				+ "\"links\":["
				+ "{"
				+ "\"source\":0,"
				+ "\"target\":1,"
				+ "\"name\":\"dc:has\","
				+ "\"right\":true,"
				+ "\"felt\":false,"
				+ "\"value\":10"
				+ "},"
				+ "{"
				+ "\"source\":0,"
				+ "\"target\":2,"
				+ "\"name\":\"dc:has\","
				+ "\"right\":true,"
				+ "\"felt\":false,"
				+ "\"value\":10"
				+ "}"
				+ "]"
				+ "}";
			Gson gson = new GsonBuilder().create();
			GraphJsonOut out = gson.fromJson(graphJsonInStr, GraphJsonOut.class);
			System.out.println(out.getNodes().get(0).getName());
	}

}


