package eu.jlinkeditrdfconverter.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.bson.types.ObjectId;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.jlinkeditrdfconverter.dao.FileUploadDAO;
import eu.jlinkeditrdfconverter.dao.GraphDAO;
import eu.jlinkeditrdfconverter.dao.UserSessionDAO;
import eu.jlinkeditrdfconverter.entity.FileUploadEntity;
import eu.jlinkeditrdfconverter.entity.GraphEntity;
import eu.jlinkeditrdfconverter.entity.LinkEntity;
import eu.jlinkeditrdfconverter.entity.NodeEntity;
import eu.jlinkeditrdfconverter.entity.UserSessionEntity;
import eu.jlinkeditrdfconverter.json.builder.JsonBuilder;
import eu.jlinkeditrdfconverter.owl.Countries;
import eu.jlinkeditrdfconverter.to.Country;


@RestController
@RequestMapping("/service")
public class JLinkeditRDFConverterService {
	
	@Autowired
	private GraphDAO graphDAO;
	
	@Autowired
	private UserSessionDAO userSessionDAO;
	
	@Autowired
	private FileUploadDAO fileUploadDAO;
	
	@Autowired
	private HttpServletRequest request;	

	
	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/loadFirstTimeGraph_", method = RequestMethod.GET)
	  public GraphEntity loadFirstTimeGraph_() throws ParseException  {
		  GraphEntity g = new GraphEntity();
		  g.setToken("token1");
		  
		  
		  
		  List<NodeEntity> nodes = new ArrayList<NodeEntity>();
		  nodes.add(new NodeEntity("Node1", "uri1", "Type1", true, 2, 100, 100));
		  nodes.add(new NodeEntity("Node2", "uri4", "Type7", false, 5, 150, 150));
		  nodes.add(new NodeEntity("Node3", "uri5", "Type8", true, 8, 200, 200));
		  g.setNodes(nodes);

		  ArrayList<LinkEntity> links = new ArrayList<LinkEntity>();
		  links.add(new LinkEntity(2, 5, "2-5", true, false, 3));
		  links.add(new LinkEntity(2, 8, "2-8", true, false, 10));
		  g.setLinks(links);
		  
		  return g;
		  
	  }
	  
	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/graph", method = RequestMethod.GET)
	  public List<GraphEntity> loadFirstTimeGraph(String search) throws ParseException  {
		  List<GraphEntity> list = graphDAO.findAll();
		  return list;
	  }

	  //@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @Consumes({MediaType.APPLICATION_JSON})
	  @RequestMapping(value="/graph", method = RequestMethod.POST, headers = "Accept=application/json")
	  public String updateGraphByExistentSession(@RequestBody GraphEntity graph) throws ParseException  {
			
		  if (graph.getToken()==null){
			  graph.setToken(userSessionDAO.askNewToken());
			  graphDAO.insert(graph);
		  } else {
			  graphDAO.update(graph);
		  }
		
		return graph.getToken();
	  }

	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/graph/{token}", method = RequestMethod.GET)
	  public GraphEntity find(@PathVariable("token") String token) {
		return graphDAO.find(token);
	  }
	  
	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/graph/{token}", method = RequestMethod.DELETE)
	  public String deleteGraph(@PathVariable("token") String token) {
		graphDAO.remove(token);
		return "removed";
	  }

	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/addNewCountry", method = RequestMethod.POST)
	  public String addNewCountry(@RequestParam(value="token") String token,@RequestParam(value="graph") String graph ,@RequestParam(value="newCountryName") String newCountryName) throws ParseException  {
		JsonBuilder jsonBuilder = new JsonBuilder();
		String graphJsonOutStr = jsonBuilder.loadNewNodeGraphBuilder(graph,newCountryName);
		
		UserSessionEntity userSessionFromDB = userSessionDAO.find(token);
		if(userSessionFromDB.getToken()==null || 
				"".equals(userSessionFromDB.getToken())||
				"".equals(userSessionFromDB.getGraphAsJsonStr())){
			userSessionFromDB.setGraphAsJsonStr(graphJsonOutStr);
			userSessionDAO.insert(userSessionFromDB);
		}
		userSessionFromDB.setGraphAsJsonStr(graphJsonOutStr);
		userSessionDAO.update(userSessionFromDB);
		
		return userSessionFromDB.getGraphAsJsonStr();
	  }
	  
	  
	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/askDefaultDataSets", method = RequestMethod.GET)
	  public String askDefaultDataSets(@RequestParam(value="token") String token, @RequestParam(value="countryNames") String countryNames) throws ParseException  {

		  System.out.println(token);
		  System.out.println(countryNames);
		  StringBuilder sb = new StringBuilder();
		  List<Country> countries = new ArrayList<Country>();
			for(Countries countryName:Countries.values()){
				countries.add(new Country(countryName.toString()));
			}
			Gson gson = new GsonBuilder().create();
			String result = gson.toJson(countries);
		return result;
	  }

	  
		//GETs ================================================================================================

	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/askNewToken_", method = RequestMethod.GET)
	  public String askNewToken_()  {
		String newToken = userSessionDAO.askNewToken();
		UserSessionEntity session = new UserSessionEntity();
		session.setToken(newToken);
		userSessionDAO.insert(session);
		System.out.println(newToken);
		return newToken;
		//return System.nanoTime() + "";
	  }
	  
	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/removeToken_", method = RequestMethod.GET)
	  public Boolean removeToken_(@RequestParam(value="token") String token)  {
		userSessionDAO.remove(token);
		UserSessionEntity session = userSessionDAO.find(token);
		if(session==null){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
		
	  }
	  
	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/findGraphByToken_", method = RequestMethod.GET)
	  public String findGraphByToken_(@RequestParam(value="token") String token)  {
		  UserSessionEntity session = userSessionDAO.find(token);
		if(session.getToken()==null || "".equals(session.getToken())){
			return null;	
		}
		return session.getGraphAsJsonStr();
	  }
	  
	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/updateGraphByToken_", method = RequestMethod.GET)
	  public Boolean updateGraphBySession_(@RequestParam(value="session") String session) throws ParseException  {
		  Gson gson = new GsonBuilder().create();
		UserSessionEntity userSessionFromJson = gson.fromJson(session,UserSessionEntity.class);
		UserSessionEntity userSessionFromDB = userSessionDAO.find(userSessionFromJson.getToken());
		if(userSessionFromDB.getToken()==null || "".equals(userSessionFromDB.getToken())){
			return null;	
		}
		userSessionFromDB.setGraphAsJsonStr(userSessionFromJson.getGraphAsJsonStr());
		userSessionDAO.update(userSessionFromDB);
		return Boolean.TRUE;
	  }
	  
	  @Consumes({MediaType.MULTIPART_FORM_DATA})
	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/fileUpload", method = RequestMethod.POST)
	  public String fileUpload() throws ParseException, IOException, ServletException, FileUploadException  {
		  
		  DiskFileItemFactory factory = new DiskFileItemFactory();
		  ServletFileUpload upload = new ServletFileUpload(factory);
		  List<FileItem> items = upload.parseRequest(request);
		  
		 FileUploadEntity fileUpload = new FileUploadEntity();
		  
		  
		 for (FileItem item: items){
			 System.out.println(item);
			 if ("token".equals(item.getFieldName())){
				 fileUpload.setToken(item.getString());
			 } else if ("description".equals(item.getFieldName())){
				 fileUpload.setDescription(item.getString());
			 } else if ("detail".equals(item.getFieldName())){
				 fileUpload.setDetail(item.getString());
			 } else if ("file".equals(item.getFieldName())){
				 fileUpload.loadFrom(item.getInputStream());
			 }
		 }
		 
		 
		 FileUploadEntity savedFileUpload = fileUploadDAO.find(fileUpload.getToken());
		 ObjectId _id = savedFileUpload == null?null: savedFileUpload.get_id();
		 if (savedFileUpload == null){
			 _id = fileUploadDAO.insert(fileUpload);
			 fileUpload.set_id(_id);
		 } else {
			 fileUpload.set_id(_id);
			 fileUploadDAO.update(fileUpload);
		 }
		
		
		return _id.toString();
		  
	  }
	  
	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/fileUpload", method = RequestMethod.GET)
	  public List<FileUploadEntity> listFileUpload() {
		return fileUploadDAO.findAll();
	  }
	  
	  @Produces({MediaType.APPLICATION_JSON})
	  @RequestMapping(value="/fileUpload/{token}", method = RequestMethod.GET)
	  public FileUploadEntity fileUpload(@PathVariable("token") String token) {
		return fileUploadDAO.find(token);
	  }
	  
	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/askNewToken", method = RequestMethod.POST)
	  public String askNewToken()  {
		String newToken = userSessionDAO.askNewToken();
		UserSessionEntity session = new UserSessionEntity();
		session.setToken(newToken);
		userSessionDAO.insert(session);
		return newToken;
	  }
	  
	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/removeToken", method = RequestMethod.POST)
	  public Boolean removeToken(@RequestParam(value="token") String token)  {
		userSessionDAO.remove(token);
		UserSessionEntity session = userSessionDAO.find(token);
		if(session==null){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
		
	  }
	  
	  @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	  @RequestMapping(value="/findGraphByToken", method = RequestMethod.POST)
	  public String findGraphByToken(@RequestParam(value="token") String token)  {
		  UserSessionEntity session = userSessionDAO.find(token);
		if(session.getToken()==null || "".equals(session.getToken())){
			return null;	
		}
		return session.getGraphAsJsonStr();
	  }
	  
}
	 	