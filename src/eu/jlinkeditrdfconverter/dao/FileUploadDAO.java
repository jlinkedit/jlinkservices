package eu.jlinkeditrdfconverter.dao;

import java.util.List;

import org.bson.types.ObjectId;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import eu.jlinkeditrdfconverter.entity.FileUploadEntity;

@Repository
public class FileUploadDAO {
	
	@Autowired
	private MongoOperations mongoOperations;
	
	public ObjectId insert(FileUploadEntity fileUpload) {
		 mongoOperations.insert(fileUpload,FileUploadEntity.class.getSimpleName());
		 
		 Query query = new Query();
		 query.addCriteria(Criteria.where("token").is(fileUpload.getToken()));
		 
		 FileUploadEntity userSessionResult = mongoOperations.findOne(query, FileUploadEntity.class,FileUploadEntity.class.getSimpleName());
		 
		 return userSessionResult.get_id();
	}

	public List<FileUploadEntity> findAll() {
		Query query = new Query();
		query.fields().include("detail").include("description").include("token");
		return mongoOperations.find(query, FileUploadEntity.class,FileUploadEntity.class.getSimpleName());
	}


	public void remove(String token) {
		Query query = new Query();
		query.addCriteria(Criteria.where("token").is(token));
		mongoOperations.remove(query, FileUploadEntity.class,FileUploadEntity.class.getSimpleName());
	}	
	
	public void remove(FileUploadEntity fileUpload) {
		Query query = new Query();
		query.addCriteria(Criteria.where("token").is(fileUpload.getToken()));
		mongoOperations.remove(query, FileUploadEntity.class,FileUploadEntity.class.getSimpleName());
	}	

	public String update(FileUploadEntity fileUpload) throws ParseException {
		if(fileUpload==null || "".equals(fileUpload.getToken()) || fileUpload.get_id()==null){
			return null;
		}
		 Query query = new Query();
			query.addCriteria(Criteria.where("token").is(fileUpload.getToken()));
		ObjectId id_ = fileUpload.get_id();
		String token = fileUpload.getToken();
		remove(fileUpload.getToken());
		//FileUploadEntity newFileUpload = new FileUploadEntity();
		fileUpload.set_id(id_);
		fileUpload.setToken(token);
		insert(fileUpload);
		return id_.toString();
	}
	
	public FileUploadEntity find(String token){
		Query query = new Query();
		query.addCriteria(Criteria.where("token").is(token));
		FileUploadEntity fileUploadResult = mongoOperations.findOne(query, FileUploadEntity.class,FileUploadEntity.class.getSimpleName());
		return fileUploadResult;
	}
	
	

//	public static void main(String[] args) throws Exception {
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:SifemCore-applicationContext.xml");
//        ISolverDAOService solverDAO = (SolverDAO) ctx.getBean("solverDAO");
//        
//        List<String> result = solverDAO.findMethodByName("PAK");
//        for(String solver:result){
//        	System.out.println(solver);
//        }
//
//        System.out.println("DONE");
//	
//	}

}
