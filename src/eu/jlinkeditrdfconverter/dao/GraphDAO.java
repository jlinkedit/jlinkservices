package eu.jlinkeditrdfconverter.dao;

import java.util.List;

import org.bson.types.ObjectId;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import eu.jlinkeditrdfconverter.entity.FileUploadEntity;
import eu.jlinkeditrdfconverter.entity.GraphEntity;
import eu.jlinkeditrdfconverter.entity.LinkEntity;
import eu.jlinkeditrdfconverter.entity.NodeEntity;


@Repository
public class GraphDAO {

	@Autowired
	private MongoOperations mongoOperations;
	
	public String insert(GraphEntity graph) {
		 mongoOperations.insert(graph,GraphEntity.class.getSimpleName());
		 
		 String token = graph.getToken();
		 
		 GraphEntity graphSaved = getOneByToken(token);
		 
		 return graphSaved.get_id().toString();
	}

	private GraphEntity getOneByToken(String token) {
		Query query = new Query();
			query.addCriteria(Criteria.where("token").is(token));
		 GraphEntity graphSaved = mongoOperations.findOne(query, GraphEntity.class,GraphEntity.class.getSimpleName());
		return graphSaved;
	}

	public List<GraphEntity> findAll() {
		Query query = new Query();
		query.fields().include("detail").include("name").include("token");
		return mongoOperations.find(query, GraphEntity.class,GraphEntity.class.getSimpleName());
	}


	public void remove(String token) {
		Query query = new Query();
		query.addCriteria(Criteria.where("token").is(token));
		mongoOperations.remove(query, GraphEntity.class,GraphEntity.class.getSimpleName());
	}	
	
	public void remove(GraphEntity graph) {
		remove(graph.getToken());
	}	

	public String update(GraphEntity graph) throws ParseException {
		if(graph==null || graph.getToken() == null || "".equals(graph.getToken().trim())){
			return null;
		}
		
		
		GraphEntity oldGraph = getOneByToken(graph.getToken());
		
		graph.set_id(oldGraph.get_id());
		
		mongoOperations.save(graph,GraphEntity.class.getSimpleName());
		
		return graph.get_id().toString();
	}
	
	public GraphEntity find(String token){
		GraphEntity graphEntity = getOneByToken(token);
		return graphEntity;
	}
	
	

//	public static void main(String[] args) throws Exception {
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:SifemCore-applicationContext.xml");
//        ISolverDAOService solverDAO = (SolverDAO) ctx.getBean("solverDAO");
//        
//        List<String> result = solverDAO.findMethodByName("PAK");
//        for(String solver:result){
//        	System.out.println(solver);
//        }
//
//        System.out.println("DONE");
//	
//	}

	
	
}
