package eu.jlinkeditrdfconverter.dao;

import java.util.List;
import java.util.UUID;

import org.bson.types.ObjectId;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import eu.jlinkeditrdfconverter.entity.UserSessionEntity;

@Repository
public class UserSessionDAO {
	
	@Autowired
	private MongoOperations mongoOperations;
	
	public String insert(UserSessionEntity userSession) {
		 mongoOperations.insert(userSession,UserSessionEntity.class.getSimpleName());
		 Query query = new Query();
			query.addCriteria(Criteria.where("token").is(userSession.getToken()));
			UserSessionEntity userSessionResult = mongoOperations.findOne(query, UserSessionEntity.class,UserSessionEntity.class.getSimpleName());
		 return userSessionResult.get_id().toString();
	}

	public List<UserSessionEntity> findAll() {
		return mongoOperations.findAll(UserSessionEntity.class,UserSessionEntity.class.getSimpleName());
	}


	public void remove(String token) {
		Query query = new Query();
		query.addCriteria(Criteria.where("token").is(token));
		mongoOperations.remove(query, UserSessionEntity.class,UserSessionEntity.class.getSimpleName());
	}	
	
	public void remove(UserSessionEntity userSession) {
		Query query = new Query();
		query.addCriteria(Criteria.where("token").is(userSession.getToken()));
		mongoOperations.remove(query, UserSessionEntity.class,UserSessionEntity.class.getSimpleName());
	}	

	public String update(UserSessionEntity userSession) throws ParseException {
		if(userSession==null || "".equals(userSession.getToken()) || userSession.get_id()==null){
			return null;
		}
		 Query query = new Query();
			query.addCriteria(Criteria.where("token").is(userSession.getToken()));
		ObjectId id_ = userSession.get_id();
		String token = userSession.getToken();
		String graph = userSession.getGraphAsJsonStr();
		remove(userSession.getToken());
		UserSessionEntity newUserSession = new UserSessionEntity();
		newUserSession.set_id(id_);
		newUserSession.setToken(token);
		newUserSession.setGraphAsJsonStr(graph);
		insert(newUserSession);
		return id_.toString();
	}
	
	
	
	public UserSessionEntity find(String token){
		Query query = new Query();
		query.addCriteria(Criteria.where("token").is(token));
		UserSessionEntity userSessionResult = mongoOperations.findOne(query, UserSessionEntity.class,UserSessionEntity.class.getSimpleName());
		return userSessionResult;
	}
	
	public String askNewToken() {
		String tokenCandidate = UUID.randomUUID().toString();
		UserSessionEntity userSessionResult = null;
		while(userSessionResult!=null){
			userSessionResult = findUserByCandidateToken(tokenCandidate);
			tokenCandidate = UUID.randomUUID().toString();
		}
		return tokenCandidate;
	}
	
	private UserSessionEntity findUserByCandidateToken(String tokenCandidate){
		Query query = new Query();
		query.addCriteria(Criteria.where("token").is(tokenCandidate));
		UserSessionEntity userSessionResult = mongoOperations.findOne(query, UserSessionEntity.class,UserSessionEntity.class.getSimpleName());
		return userSessionResult;
	}
//	public static void main(String[] args) throws Exception {
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:SifemCore-applicationContext.xml");
//        ISolverDAOService solverDAO = (SolverDAO) ctx.getBean("solverDAO");
//        
//        List<String> result = solverDAO.findMethodByName("PAK");
//        for(String solver:result){
//        	System.out.println(solver);
//        }
//
//        System.out.println("DONE");
//	
//	}




}
