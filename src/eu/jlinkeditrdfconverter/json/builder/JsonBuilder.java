package eu.jlinkeditrdfconverter.json.builder;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.jlinkeditrdfconverter.entity.GraphEntity;
import eu.jlinkeditrdfconverter.owl.Countries;
import eu.jlinkeditrdfconverter.owl.Europe;
import eu.jlinkeditrdfconverter.owl.builder.BuilderUtil;
import eu.jlinkeditrdfconverter.service.JLinkeditRDFConverterServiceUtil;
import eu.jvisualrdf.graph.json.in.GraphJsonIn;
import eu.jvisualrdf.graph.json.out.GraphJsonOut;
import eu.jvisualrdf.graph.json.out.Link;
import eu.jvisualrdf.graph.json.out.Node;

public class JsonBuilder {
	
	public String loadFirstTimeGraphBuilder(){
		GraphJsonOut graphJsonOut = new GraphJsonOut();
		List<Node> nodes = new ArrayList<Node>();
		Node europeNode = new Node();
		europeNode.setId(0);
		europeNode.setName("Europe Union");
		europeNode.setReflexive(Boolean.TRUE);
		europeNode.setType("Resource");
		europeNode.setUri(BuilderUtil.getAbout(new Europe()));
		nodes.add(europeNode);
		
		
		List<Link> links = new ArrayList<Link>();
				
			
			
			Node node = new Node();
			node.setId(1);
			node.setName(Countries.Ireland.name());
			node.setReflexive(Boolean.FALSE);
			node.setType("value");
			node.setUri("");
			
			

			Link link = new Link();
			link.setFelt(Boolean.FALSE);
			link.setName("dc:has");
			link.setRight(Boolean.TRUE);
			link.setValue(10);
			link.setSource(0);
			link.setTarget(1);
			
			
			//
			

			
			Node node2 = new Node();
			node2.setId(2);
			node2.setName(Countries.United_Kingdom.name());
			node2.setReflexive(Boolean.FALSE);
			node2.setType("value");
			node2.setUri("");
			
			Link link2 = new Link();
			link2.setFelt(Boolean.FALSE);
			link2.setName("dc:has");
			link2.setRight(Boolean.TRUE);
			link2.setValue(10);
			link2.setSource(0);
			link2.setTarget(2);
			
			
			
			nodes.add(node);
			links.add(link);
			nodes.add(node2);
			links.add(link2);

		graphJsonOut.addLinkinOut(links);
		graphJsonOut.addNodeOut(nodes);
		
		Gson gson = new GsonBuilder().create();
		String result = gson.toJson(graphJsonOut);
		
		return result;
	}
	
	public GraphJsonOut loadNewNodeGraphBuilderObj(String graphJsonInStr,String newCountryName){
		
		GraphJsonIn graphJsonIn = JLinkeditRDFConverterServiceUtil.convertGraphJsonIn(graphJsonInStr);
		GraphJsonOut graphJsonOut = JLinkeditRDFConverterServiceUtil.parseGraphJsonInToOut(graphJsonIn);
		List<Node> nodes = (graphJsonOut==null || graphJsonOut.getNodes()==null||graphJsonOut.getNodes().isEmpty())?null:graphJsonOut.getNodes();
		List<Link> links = (graphJsonOut==null || graphJsonOut.getLinks()==null||graphJsonOut.getNodes().isEmpty())?null:graphJsonOut.getLinks();
		

			
			int id = nodes.size();
			Node node = new Node();
			node.setId(id);
			node.setName(newCountryName);
			node.setReflexive(Boolean.FALSE);
			node.setType("value");
			node.setUri("");
			
			Link link = new Link();
			link.setValue(10);
			link.setFelt(Boolean.FALSE);
			link.setName("dc:has");
			link.setRight(Boolean.TRUE);
			link.setSource(0);
			link.setTarget(id);
			
			
			nodes.add(node);
			links.add(link);


			graphJsonOut.addLinkinOut(links);
			graphJsonOut.addNodeOut(nodes);
		
		return graphJsonOut;
	}
	
	public String loadNewNodeGraphBuilder(String graphJsonInStr,String newCountryName){
	
		GraphJsonIn graphJsonIn = JLinkeditRDFConverterServiceUtil.convertGraphJsonIn(graphJsonInStr);
		GraphJsonOut graphJsonOut = JLinkeditRDFConverterServiceUtil.parseGraphJsonInToOut(graphJsonIn);
		List<Node> nodes = (graphJsonOut==null || graphJsonOut.getNodes()==null||graphJsonOut.getNodes().isEmpty())?null:graphJsonOut.getNodes();
		List<Link> links = (graphJsonOut==null || graphJsonOut.getLinks()==null||graphJsonOut.getLinks().isEmpty())?null:graphJsonOut.getLinks();
		
		
			int id = nodes.size();

			
			
			Node node = new Node();
			node.setId(id);
			node.setName(newCountryName);
			node.setReflexive(Boolean.FALSE);
			node.setType("value");
			node.setUri("");
			
			Link link = new Link();
			link.setFelt(Boolean.FALSE);
			link.setName("dc:has");
			link.setRight(Boolean.TRUE);
			link.setValue(10);
			link.setSource(0);
			link.setTarget(id);
			
			
			
//			List<Node> newListNodes = new ArrayList<Node>();
//			newListNodes.addAll(nodes);
//			for(Node nodeLoop: nodes){
//				if(nodeLoop.getName().trim().toUpperCase().equals(node.getName().trim().toUpperCase())){
//					continue;
//				}else{
//					newListNodes.add(node);
//				}
//			}
//			
//			List<Link> newListLinks = new ArrayList<Link>();
//			newListLinks.addAll(links);
//			for(Link linkLoop:links){
//				if(linkLoop.getName().trim().toUpperCase().equals(link.getName().trim().toUpperCase())){
//					continue;
//				}else{
//					newListLinks.add(link);
//				}
//			}
			
			nodes.add(node);
			links.add(link);


			graphJsonOut.addLinkinOut(links);
			graphJsonOut.addNodeOut(nodes);

			
			Gson gson = new GsonBuilder().create();
			String result = gson.toJson(graphJsonOut);
		
		return result;
	}

//	public GraphEntity parseGraphOutToEntity(GraphJsonOut graphJsonOut,String token) {
//		GraphEntity entity = new GraphEntity();
//		entity.set_id(graphJsonOut);
//		
//		return null;
//	}
	

}
