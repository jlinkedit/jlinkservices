function graphApi() {

	// private function
	var getHostLocation = function() {
		var fileName  	= "fileName";
		var stack       = "stack";
		var stackTrace  = "stacktrace";
		var loc   	= null;

		var matcher = function(stack, matchedLoc) { return loc = matchedLoc; };

		try {

			// Invalid code
			0();

		}  catch (ex) {

			if(fileName in ex) { // Firefox
				loc = ex[fileName];
			} else if(stackTrace in ex) { // Opera
				ex[stackTrace].replace(/called from line \d+, column \d+ in (.*):/gm, matcher);
			} else if(stack in ex) { // WebKit, Blink and IE10
				ex[stack].replace(/at.*?\(?(\S+):\d+:\d+\)?$/g, matcher);
			}
			loc = loc.substr(0, loc.indexOf('api/'));
			return loc;
		}
	};

	this.host = getHostLocation();

}

graphApi.prototype = {
		graphs: function(term, callback){
			var data ={"search":term};
			$.getJSON(this.host + "rest/service/graph", data, callback);
		},

		saveGraph: function(graph, callback){

			$.ajax({
			    url: this.host + "rest/service/graph",
			    type: 'POST',
			    dataType: 'json',
			    data: JSON.stringify(graph),
			    contentType: 'application/json',
			    mimeType: 'application/json',
			    success: function(data) {
			        callback(data);
			    }
		  });
		},

		loadGraph: function(token, callback){
			$.getJSON(this.host + "rest/service/graph/" + token, callback);
		},

		removeGraph: function(token, callback){
			$.ajax({
	    	url: this.host + "rest/service/graph/" + token,
	    	type: 'DELETE',
	    	success: callback
		  });
		},

		uploadCsv: function(form, callback){
			var i = form[0];

			var formData = new FormData(i);

			$.ajax({
				url: this.host + 'rest/service/fileUpload',
				type: 'POST',
				data: formData,
				async: false,
				cache: false,
				contentType: false,
				enctype: 'multipart/form-data',
				processData: false,
				success: function (response) {
					callback(response);
				}
			});

		},

		listDataSources: function(callback){
			$.getJSON(this.host + "rest/service/fileUpload",callback);
		},

		listEntriesOf: function(token, callback){
			$.getJSON(this.host + "rest/service/fileUpload/" + token,callback);
		},

		toD3: function(graph){
			var gd3 = $.extend(true, {lastNodeId:0, nodeMap:{}}, graph);
			var ids = {};

			gd3.nodes.filter(function(item){
				return item?true:false;
			}).forEach(function(item, index) {
						  gd3.nodeMap[item.name] = {position:index};
						  gd3.lastNodeId = gd3.lastNodeId < item.id?item.id:gd3.lastNodeId;
							ids[item.id] = item;
			});

			gd3.links.filter(function(link){
					return ids[link.source] && ids[link.target];
			}).forEach(function(link){
					link.source = ids[link.source];
					link.target = ids[link.target];
			});
			return gd3;
		},
		fromD3: function(gd3){
			var temp = $.extend(true, {}, gd3);
			var graph = {};
			graph.nodes =  temp.nodes;
			graph.links =  temp.links;

			if (temp.token){
				graph.token = temp.token;
		  }

			graph.name = temp.name;
			graph.detail = temp.detail;

			graph.nodes.forEach(function(node, index) {
				node.x = node.x | 0;
				node.y = node.y | 0;


				delete node['px'];
				delete node['py'];

				delete node['index'];
				delete node['weight'];
			});

			graph.links.forEach(function(link) {
				link.source = link.source.id;
				link.target = link.target.id;
			});

			return graph;
		}
};

Graphs = new graphApi();
